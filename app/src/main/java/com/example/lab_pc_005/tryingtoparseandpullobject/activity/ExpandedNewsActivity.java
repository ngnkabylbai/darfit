package com.example.lab_pc_005.tryingtoparseandpullobject.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.lab_pc_005.tryingtoparseandpullobject.R;
import com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ExpandedNewsActivity extends AppCompatActivity {

    @BindView(R.id.v_news_title_exp) TextView title;
    @BindView(R.id.v_news_content_exp) TextView content;
    @BindView(R.id.v_news_author_exp) TextView author;
    @BindView(R.id.v_news_dateTime_exp) TextView dateTime;
    @BindView(R.id.v_news_image_exp) ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expanded_news);
        ButterKnife.bind(this);

        Intent intent = getIntent();

        ButterKnife.bind(this);
        title.setText(intent.getStringExtra("title"));
        content.setText(intent.getStringExtra("content"));
        author.setText(intent.getStringExtra("author"));
        dateTime.setText(intent.getStringExtra("dateTime"));

        Glide.with(this).load(intent.getStringExtra("imageURL")).into(image);

    }

    @OnClick(R.id.backButton)
    public void backButtonPressed() {
        finish();
    }
}
