package com.example.lab_pc_005.tryingtoparseandpullobject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lab_pc_005.tryingtoparseandpullobject.R;
import com.example.lab_pc_005.tryingtoparseandpullobject.entry.Exercise;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lab-pc-005 on 6/19/17.
 */

public class ScheduleRVAdapter extends RecyclerView.Adapter<ScheduleRVAdapter.ScheduleViewHolder> {

    private ArrayList<Exercise> arrayOfExercises;
    private Context context;

    public ScheduleRVAdapter(ArrayList<Exercise> arrayOfExercises, Context context) {
        this.arrayOfExercises = arrayOfExercises;
        this.context = context;
    }

    @Override
    public ScheduleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ScheduleViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_schedule, parent, false));
    }

    @Override
    public void onBindViewHolder(ScheduleViewHolder holder, int position) {
        holder.time.setText(arrayOfExercises.get(position).getTime());
        holder.sectionName.setText(arrayOfExercises.get(position).getSectionName());
        holder.sectionDescription.setText(arrayOfExercises.get(position).getSectionDescription());
    }

    @Override
    public int getItemCount() {
        return arrayOfExercises.size();
    }

    class ScheduleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.schd_time) TextView time;
        @BindView(R.id.schd_name) TextView sectionName;
        @BindView(R.id.schd_description) TextView sectionDescription;

        private ScheduleViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }

}
