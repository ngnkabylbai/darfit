package com.example.lab_pc_005.tryingtoparseandpullobject.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lab_pc_005.tryingtoparseandpullobject.adapter.NewsRVAdapter;
import com.example.lab_pc_005.tryingtoparseandpullobject.R;
import com.example.lab_pc_005.tryingtoparseandpullobject.entry.News;
import com.example.lab_pc_005.tryingtoparseandpullobject.util.DARFITDBHelper;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends Fragment {

    private NewsRVAdapter adapter;
    private DatabaseReference mRef;
    private ArrayList<News> arrayOfNews;

    public NewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_news, container, false);
        arrayOfNews = new ArrayList<>();
        mRef = FirebaseDatabase.getInstance().getReference("News");
        adapter = new NewsRVAdapter(arrayOfNews, rootView.getContext());

        RecyclerView rv = (RecyclerView) rootView.findViewById(R.id.rv_news);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        rv.setAdapter(adapter);

        fillArrayOfNews();

        return rootView;
    }

    private void fillArrayOfNews() {
        mRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                News pulled = dataSnapshot.getValue(News.class);
                arrayOfNews.add(pulled);
                adapter.notifyDataSetChanged();
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }
}
