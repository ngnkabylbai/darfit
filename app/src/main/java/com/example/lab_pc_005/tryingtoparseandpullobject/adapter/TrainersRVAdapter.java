package com.example.lab_pc_005.tryingtoparseandpullobject.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lab_pc_005.tryingtoparseandpullobject.R;
import com.example.lab_pc_005.tryingtoparseandpullobject.activity.ExpandedTrainerActivity;
import com.example.lab_pc_005.tryingtoparseandpullobject.entry.Trainer;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TAG;

/**
 * Created by lab-pc-005 on 6/22/17.
 */

public class TrainersRVAdapter extends RecyclerView.Adapter<TrainersRVAdapter.TrainersViewHolder> {

    private ArrayList<Trainer> arrayOfTrainers;
    private Context context;

    public TrainersRVAdapter(ArrayList<Trainer> arrayOfTrainers, Context context) {
        this.arrayOfTrainers = arrayOfTrainers;
        this.context = context;
    }

    @Override
    public TrainersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TrainersViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_less_trainers, parent, false));
    }

    @Override
    public void onBindViewHolder(TrainersViewHolder holder, int position) {
        Trainer curTrainer = arrayOfTrainers.get(position);
        ((TextView) holder.fullName).setText(curTrainer.getName()
                +" "+ curTrainer.getSurname());
        ((TextView) holder.description).setText(curTrainer.getDescription());
        ((TextView) holder.phoneNumber).setText(curTrainer.getPhoneNumber());

        // TODO: donwload url not_signin_main by Glide
    }

    @Override
    public int getItemCount() {
        return arrayOfTrainers.size();
    }

    private ArrayList<Trainer> getArrayOfTrainers() {
        return arrayOfTrainers;
    }

    private Context getContext() { return context; }

    class TrainersViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.v_less_trainers_image) ImageView image;
        @BindView(R.id.v_less_trainers_fullName) TextView fullName;
        @BindView(R.id.v_less_trainers_description) TextView description;
        @BindView(R.id.v_less_trainers_phoneNumber) TextView phoneNumber;

        TrainersViewHolder (View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                    TODO: intent
                    Trainer curTrainer = getArrayOfTrainers().get(getAdapterPosition());
                    Intent intent = new Intent(getContext(), ExpandedTrainerActivity.class);

                    intent.putExtra("ID", curTrainer.getID());
                    Log.d(TAG, String.valueOf(curTrainer.getID()));

                    getContext().startActivity(intent);
                }
            });
        }
    }
}
