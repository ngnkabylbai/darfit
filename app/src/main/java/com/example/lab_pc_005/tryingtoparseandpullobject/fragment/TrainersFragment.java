package com.example.lab_pc_005.tryingtoparseandpullobject.fragment;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lab_pc_005.tryingtoparseandpullobject.R;
import com.example.lab_pc_005.tryingtoparseandpullobject.adapter.TrainersRVAdapter;
import com.example.lab_pc_005.tryingtoparseandpullobject.entry.Trainer;
import com.example.lab_pc_005.tryingtoparseandpullobject.util.DARFITDBHelper;

import java.util.ArrayList;

import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TAG;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_DESCRIPTION;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_ID;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_NAME;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_PHONE_NUMBER;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_SURNAME;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_URL_MAIN;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrainersFragment extends Fragment {

    private DARFITDBHelper dbHelper;
    private TrainersRVAdapter adapter;
    private ArrayList<Trainer> arrayOfTrainers;

    public TrainersFragment() {
        // Required empty public constructor
    }

    // TODO: push data into Firebase
    // TODO: set NULL values to null cells in the database

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_trainers, container, false);
        arrayOfTrainers = new ArrayList<>();
        adapter = new TrainersRVAdapter(arrayOfTrainers, rootView.getContext());
        dbHelper = new DARFITDBHelper(rootView.getContext());

        RecyclerView rv = (RecyclerView) rootView.findViewById(R.id.rv_trainers);
        rv.setAdapter(adapter);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(rootView.getContext()));

        getListOfTrainers();

        return rootView;
    }

    private void getListOfTrainers() {
        Cursor c = DARFITDBHelper.getTrainersCursor(dbHelper.getReadableDatabase());

        // TODO: remove the comment
        if (c.moveToFirst()) {
            do {
                arrayOfTrainers.add(new Trainer(
                        c.getInt(c.getColumnIndex(TRAINERS_COLUMN_ID)),
                        c.getString(c.getColumnIndex(TRAINERS_COLUMN_NAME)),
                        c.getString(c.getColumnIndex(TRAINERS_COLUMN_SURNAME)),
                        c.getString(c.getColumnIndex(TRAINERS_COLUMN_DESCRIPTION)),
                        c.getString(c.getColumnIndex(TRAINERS_COLUMN_PHONE_NUMBER)),
                        c.getString(c.getColumnIndex(TRAINERS_COLUMN_URL_MAIN))));
            } while (c.moveToNext());
            c.close();
            adapter.notifyDataSetChanged();
        } else {
            Log.d(TAG, "TRAINERS: Cursor is empty");
        }
    }

}
