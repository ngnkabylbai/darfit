package com.example.lab_pc_005.tryingtoparseandpullobject.entry;

/**
 * Created by lab-pc-005 on 6/19/17.
 */

public class Exercise {

    private String time;
    private String sectionName;
    private String sectionDescription;
    private String trainerName;

    // TODO add a trainer here

    public Exercise(String time, String sectionName, String sectionDescription) {
        this.time = time;
        this.sectionDescription = sectionDescription;
        this.sectionName = sectionName;
    }

    public String getTime() {
        return time;
    }

    public String getSectionName() {
        return sectionName;
    }

    public String getSectionDescription() {
        return sectionDescription;
    }

    public String getTrainerName() {
        return trainerName;
    }
}
