package com.example.lab_pc_005.tryingtoparseandpullobject.util;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.DEF_SCHEDULE_COLUMN_HOUR;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.DEF_SCHEDULE_COLUMN_ID;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.DEF_SCHEDULE_COLUMN_WEEKDAY;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTIONS_COLUMN_DESCRIPTION;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTIONS_COLUMN_ID;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTIONS_COLUMN_NAME;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTIONS_COLUMN_URL_MAIN;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTION_ADD_COLUMN_DEF_SCHEDULE_ID;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTION_ADD_COLUMN_ID;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTION_ADD_COLUMN_SECTION_ID;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTION_ADD_COLUMN_TRAINER_ID;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TABLE_NAME_DEF_SCHEDULE;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TABLE_NAME_SECTIONS;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TABLE_NAME_SECTION_ADDITIONAL;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TABLE_NAME_TRAINERS;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TAG;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_DESCRIPTION;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_ID;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_NAME;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_PHONE_NUMBER;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_SURNAME;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_URL_MAIN;


/**
 * Created by lab-pc-005 on 6/15/17.
 */

public class DARFITDBHelper extends SQLiteOpenHelper {

    // TODO: Add trainers to sections by creating another table

    private static final String DATABASE_NAME = "DARFIT";
    private static int DATABASE_VERSION = 6;

    private static final String SQL_CREATE_TABLE_TRAINERS =
            "CREATE TABLE " + TABLE_NAME_TRAINERS + "("
                    + TRAINERS_COLUMN_ID + " INTEGER PRIMARY KEY, "
                    + TRAINERS_COLUMN_NAME + " TEXT, "
                    + TRAINERS_COLUMN_SURNAME + " TEXT, "
                    + TRAINERS_COLUMN_PHONE_NUMBER + " TEXT, "
                    + TRAINERS_COLUMN_DESCRIPTION + " TEXT, "
                    + TRAINERS_COLUMN_URL_MAIN + " TEXT)";

    private static final String SQL_CREATE_TABLE_SECTIONS =
            "CREATE TABLE " + TABLE_NAME_SECTIONS + "("
                    + SECTIONS_COLUMN_ID + " INTEGER PRIMARY KEY, "
                    + SECTIONS_COLUMN_NAME + " TEXT, "
                    + SECTIONS_COLUMN_DESCRIPTION + " TEXT,"
                    + SECTIONS_COLUMN_URL_MAIN + " TEXT)";

    private static final String SQL_CREATE_TABLE_DEF_SCHEDULE =
            "CREATE TABLE " + TABLE_NAME_DEF_SCHEDULE + "("
                    + DEF_SCHEDULE_COLUMN_ID + " INTEGER PRIMARY KEY, "
                    + DEF_SCHEDULE_COLUMN_WEEKDAY + " INTEGER,"
                    + DEF_SCHEDULE_COLUMN_HOUR + " TEXT)";

    private static final String SQL_CREATE_TABLE_SECTION_ADDITIONAL =
            "CREATE TABLE " + TABLE_NAME_SECTION_ADDITIONAL + "("
                    + SECTION_ADD_COLUMN_ID + " INTEGER PRIMARY KEY, "
                    + SECTION_ADD_COLUMN_DEF_SCHEDULE_ID + " TEXT, "
                    + SECTION_ADD_COLUMN_SECTION_ID + " TEXT, "
                    + SECTION_ADD_COLUMN_TRAINER_ID + " TEXT)";

    private static final String SQL_DROP_TRAINERS =
            "DROP TABLE IF EXISTS " + TABLE_NAME_TRAINERS;
    private static final String SQL_DROP_SECTIONS =
            "DROP TABLE IF EXISTS " + TABLE_NAME_SECTIONS;
    private static final String SQL_DROP_DEF_SCHEDULE =
            "DROP TABLE IF EXISTS " + TABLE_NAME_DEF_SCHEDULE;
    private static final String SQL_DROP_SECTION_ADDITIONAL =
            "DROP TABLE IF EXISTS " + TABLE_NAME_SECTION_ADDITIONAL;

    public DARFITDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_TRAINERS);
        db.execSQL(SQL_CREATE_TABLE_SECTIONS);
        db.execSQL(SQL_CREATE_TABLE_DEF_SCHEDULE);
        db.execSQL(SQL_CREATE_TABLE_SECTION_ADDITIONAL);

        // TODO: is it good practice?!

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "DARFITDBHelper onUpgrade");
        db.execSQL(SQL_DROP_TRAINERS);
        db.execSQL(SQL_DROP_SECTIONS);
        db.execSQL(SQL_DROP_DEF_SCHEDULE);
        db.execSQL(SQL_DROP_SECTION_ADDITIONAL);
        onCreate(db);
    }

    public int getDatabaseVersion() {
        return DATABASE_VERSION;
    }

    public static Cursor getTrainersCursor(SQLiteDatabase db) {
        return db.query(TABLE_NAME_TRAINERS, null, null, null, null, null, null);
    }

    public static Cursor getSectionsCursor(SQLiteDatabase db) {
        return db.query(TABLE_NAME_SECTIONS, null, null, null, null, null, null);
    }

    public static Cursor getDefScheduleCursor(SQLiteDatabase db) {
        return db.query(TABLE_NAME_DEF_SCHEDULE, null, null, null, null, null, null);
    }

    public static Cursor getAddDataCursor(SQLiteDatabase db) {
        return db.query(TABLE_NAME_SECTION_ADDITIONAL, null, null, null, null, null, null);
    }

    public static Cursor getJoinedCursor(SQLiteDatabase db, int weekDay) {
        String sqlQuery = "select " + DEF_SCHEDULE_COLUMN_WEEKDAY
                + ", " + DEF_SCHEDULE_COLUMN_HOUR
                + ", " + SECTIONS_COLUMN_NAME
                + ", " + SECTIONS_COLUMN_DESCRIPTION
                + " from " + TABLE_NAME_DEF_SCHEDULE + " as defSchdTable"
                + " inner join " + TABLE_NAME_SECTION_ADDITIONAL + " as addTable"
                + " on addTAble." + SECTION_ADD_COLUMN_DEF_SCHEDULE_ID + "=defSchdTable." + DEF_SCHEDULE_COLUMN_ID
                + " inner join " + TABLE_NAME_SECTIONS + " as sectionsTable"
                + " on sectionsTable." + SECTIONS_COLUMN_ID + "=addTAble." + SECTION_ADD_COLUMN_SECTION_ID;

        if (weekDay != -1) {
            sqlQuery += " where " + DEF_SCHEDULE_COLUMN_WEEKDAY + "=" + weekDay;
        }

        return db.rawQuery(sqlQuery, null);
    }

    public static Cursor getCursorById(SQLiteDatabase db, String tableName, int ID) {
        String sqlQuery = "select *"
                + " from " + tableName
                + " where ID=" + ID;

        return db.rawQuery(sqlQuery, null);
    }

    public static void logCursor(Cursor c) {
        if (c != null) {
            if (c.moveToFirst()) {
                String str;
                do {
                    str = "";
                    for (String cn : c.getColumnNames()) {
                        str = str.concat(cn + " = " + c.getString(c.getColumnIndex(cn)) + "; ");
                    }
                    Log.d(TAG, str);
                } while (c.moveToNext());
            }
        } else
            Log.d(TAG, "Cursor is null");
    }

}
