package com.example.lab_pc_005.tryingtoparseandpullobject.entry;

/**
 * Created by lab-pc-005 on 6/15/17.
 */

public class Section {

    private int ID;
    private String name;
    private String description;
    private String URLMain;

    public Section () {

    }

    public Section (int ID, String name, String description, String URLMain) {
        this.ID = ID;
        this.name = name;
        this.URLMain = URLMain;
        this.description = description;
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public String getURLMain() {
        return URLMain;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Section{" +
                "name='" + name + '\'' +
                '}';
    }
}
