package com.example.lab_pc_005.tryingtoparseandpullobject.util;

/**
 * Created by lab-pc-005 on 6/16/17.
 */

public class StringExtras {

    public static final String TAG = "myLogs";

    public static final String TABLE_NAME_SECTIONS = "Sections";
    public static final String TABLE_NAME_TRAINERS = "Trainers";
    public static final String TABLE_NAME_DEF_SCHEDULE = "DefaultSchedule";
    public static final String TABLE_NAME_SECTION_ADDITIONAL = "SectionAdditional";

    public static final String SECTIONS_COLUMN_ID = "ID";
    public static final String SECTIONS_COLUMN_NAME = "Name";
    public static final String SECTIONS_COLUMN_DESCRIPTION = "Description";
    public static final String SECTIONS_COLUMN_URL_MAIN = "URLMain";

    public static final String TRAINERS_COLUMN_ID = "ID";
    public static final String TRAINERS_COLUMN_NAME = "Name";
    public static final String TRAINERS_COLUMN_SURNAME = "Surname";
    public static final String TRAINERS_COLUMN_DESCRIPTION = "Description";
    public static final String TRAINERS_COLUMN_PHONE_NUMBER = "PhoneNumber";
    public static final String TRAINERS_COLUMN_URL_MAIN = "URLMain";


    public static final String DEF_SCHEDULE_COLUMN_ID = "_id";
    public static final String DEF_SCHEDULE_COLUMN_WEEKDAY = "WeekDay";
    public static final String DEF_SCHEDULE_COLUMN_HOUR = "Hour";

    public static final String SECTION_ADD_COLUMN_ID = "_id";
    public static final String SECTION_ADD_COLUMN_DEF_SCHEDULE_ID = "DefaultScheduleID";
    public static final String SECTION_ADD_COLUMN_SECTION_ID = "SectionID";
    public static final String SECTION_ADD_COLUMN_TRAINER_ID = "TrainerID";

    // firebase keys

    public static final String FRBS_SECTION_ID = "section_id";
    public static final String FRBS_TRAINER_ID = "trainer_id";

    // SQLite
    public static final String SQL_DEF_SCHD_LASTID = "SELECT max(_id) from " + TABLE_NAME_DEF_SCHEDULE;


    ////////////////////////////////////
    public static final int REQUEST_CODE = 1;
    public static final int RESULT_OK = 1;
    public static final int RESULT_ERR = 2;

    public static final String USER_NAME = "user_name";
    public static final String USER_EMAIL = "user_email";
}

