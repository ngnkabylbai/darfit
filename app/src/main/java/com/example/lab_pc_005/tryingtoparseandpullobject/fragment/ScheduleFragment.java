package com.example.lab_pc_005.tryingtoparseandpullobject.fragment;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lab_pc_005.tryingtoparseandpullobject.R;
import com.example.lab_pc_005.tryingtoparseandpullobject.adapter.ScheduleRVAdapter;
import com.example.lab_pc_005.tryingtoparseandpullobject.entry.Exercise;
import com.example.lab_pc_005.tryingtoparseandpullobject.util.DARFITDBHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarListener;

import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.DEF_SCHEDULE_COLUMN_HOUR;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTIONS_COLUMN_DESCRIPTION;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTIONS_COLUMN_NAME;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class ScheduleFragment extends Fragment {

    private View rootView;
    private DARFITDBHelper dbHelper;
    private ScheduleRVAdapter adapter;
    private ArrayList<Exercise> arrayOfExercises;

    @BindView(R.id.holiday_image) ImageView holidayImage;
    @BindView(R.id.holiday_text) TextView holidayText;

    private final int HOLIDAY_IMAGE_RES = R.drawable.holiday_smile;
    private final int HOLIDAY_TEXT_RES = R.string.today_is_a_holiday;
    private final int HOLIDAY_IMAGE_REMOVE = 0;
    private final String HOLIDAY_TEXT_REMOVE = "";

    public ScheduleFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_schedule, container, false);
        ButterKnife.bind(this, rootView);
        arrayOfExercises = new ArrayList<>();
        dbHelper = new DARFITDBHelper(rootView.getContext());
        adapter = new ScheduleRVAdapter(arrayOfExercises, rootView.getContext());

        RecyclerView rv = (RecyclerView) rootView.findViewById(R.id.rv_schedule);
        rv.setAdapter(adapter);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));

        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);

        HorizontalCalendar calendar = new HorizontalCalendar.Builder(rootView, R.id.schedule_calendar)
                                    .startDate(startDate.getTime())
                                    .endDate(endDate.getTime())
                                    .build();


        calendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Date date, int position) {
                notHoliday();

                int weekday = 1;
                try {
                    weekday = date.getDay();
                    Log.d(TAG, "WEEKDAY: " + weekday);

                    arrayOfExercises.clear();
                    Cursor c = DARFITDBHelper.getJoinedCursor(dbHelper.getReadableDatabase(), weekday);

                    if (c.moveToFirst()) {
                        do {
                            arrayOfExercises.add(new Exercise(
                                            c.getString(c.getColumnIndex(DEF_SCHEDULE_COLUMN_HOUR)),
                                            c.getString(c.getColumnIndex(SECTIONS_COLUMN_NAME)),
                                            c.getString(c.getColumnIndex(SECTIONS_COLUMN_DESCRIPTION))
                                    )
                            );

                        } while (c.moveToNext());
                        c.close();
                    } else {
                        isHoliday();
                    }

                    adapter.notifyDataSetChanged();
                } catch (NumberFormatException e) {
                    e.printStackTrace();

                }
            }
        });

        return rootView;
    }

    private void isHoliday() {
        holidayImage.getLayoutParams().height = (int) getResources().getDimension(R.dimen.holiday_image_height);
        holidayImage.setImageResource(HOLIDAY_IMAGE_RES);
        holidayText.setText(HOLIDAY_TEXT_RES);
    }

    private void notHoliday() {
        holidayImage.setImageResource(HOLIDAY_IMAGE_REMOVE);
        holidayImage.getLayoutParams().height = HOLIDAY_IMAGE_REMOVE;
        holidayText.setText(HOLIDAY_TEXT_REMOVE);
    }
}
