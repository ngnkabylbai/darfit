package com.example.lab_pc_005.tryingtoparseandpullobject;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.lab_pc_005.tryingtoparseandpullobject.activity.MainActivity;
import com.example.lab_pc_005.tryingtoparseandpullobject.util.DARFITDBHelper;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.DEF_SCHEDULE_COLUMN_HOUR;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.DEF_SCHEDULE_COLUMN_WEEKDAY;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.FRBS_SECTION_ID;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTIONS_COLUMN_DESCRIPTION;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTIONS_COLUMN_ID;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTIONS_COLUMN_NAME;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTIONS_COLUMN_URL_MAIN;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTION_ADD_COLUMN_DEF_SCHEDULE_ID;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTION_ADD_COLUMN_SECTION_ID;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTION_ADD_COLUMN_TRAINER_ID;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SQL_DEF_SCHD_LASTID;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TABLE_NAME_DEF_SCHEDULE;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TABLE_NAME_SECTIONS;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TABLE_NAME_SECTION_ADDITIONAL;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TABLE_NAME_TRAINERS;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TAG;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_DESCRIPTION;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_ID;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_NAME;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_PHONE_NUMBER;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_SURNAME;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_URL_MAIN;

public class SplashActivity extends AppCompatActivity {

    private DatabaseReference sectionsRef = FirebaseDatabase.getInstance().getReference("Sections");
    private DatabaseReference trainersRef = FirebaseDatabase.getInstance().getReference("Trainers");
    private DatabaseReference schRef = FirebaseDatabase.getInstance().getReference("Schedule");
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        db = new DARFITDBHelper(this).getWritableDatabase();

//        startMainActivity();
        updateDatabase();
    }

    //TODO: change it
    private void updateDatabase() {
        clearDatabase();
        pullSections();
    }

    private void clearDatabase() {
        db.delete(TABLE_NAME_DEF_SCHEDULE, null, null);
        db.delete(TABLE_NAME_SECTION_ADDITIONAL, null, null);
        db.delete(TABLE_NAME_SECTIONS, null, null);
        db.delete(TABLE_NAME_TRAINERS, null, null);
    }

    private void pullSections() {

        sectionsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot sectionChild : dataSnapshot.getChildren()) {
                    ContentValues curValues = new ContentValues();
                    curValues.put(SECTIONS_COLUMN_ID,
                            Integer.valueOf(sectionChild.child("section_id").getValue(String.class)));
                    curValues.put(SECTIONS_COLUMN_NAME,
                            sectionChild.child("name").getValue(String.class));
                    curValues.put(SECTIONS_COLUMN_DESCRIPTION,
                            sectionChild.child("description").getValue(String.class));
                    curValues.put(SECTIONS_COLUMN_URL_MAIN,
                            sectionChild.child("url-not_signin_main").getValue(String.class));

                    db.insert(TABLE_NAME_SECTIONS, null, curValues);
                }
                pullTrainers(db);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, String.valueOf(databaseError.toException()));
            }
        });
    }

    private void pullTrainers(final SQLiteDatabase db) {

        trainersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot trainerChild : dataSnapshot.getChildren()) {

                    // TODO: end parsing
                    if (trainerChild.getKey().equals("changed")) {
                        Log.d(TAG, "CHANGED STATUS: " + trainerChild.getValue());

                    } else {
                        ContentValues curValues = new ContentValues();
                        curValues.put(TRAINERS_COLUMN_ID,
                                trainerChild.child("id").getValue(Integer.class));
                        curValues.put(TRAINERS_COLUMN_NAME,
                                trainerChild.child("name").getValue(String.class));
                        curValues.put(TRAINERS_COLUMN_SURNAME,
                                trainerChild.child("surname").getValue(String.class));
                        curValues.put(TRAINERS_COLUMN_DESCRIPTION,
                                trainerChild.child("description").getValue(String.class));
                        curValues.put(TRAINERS_COLUMN_PHONE_NUMBER,
                                trainerChild.child("mobile-number").getValue(String.class));
                        curValues.put(TRAINERS_COLUMN_URL_MAIN,
                                trainerChild.child("url-not_signin_main").getValue(String.class));

                        db.insert(TABLE_NAME_TRAINERS, null, curValues);
                    }
                }
                pullDefaultSchedule(db);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, String.valueOf(databaseError.toException()));
            }
        });
    }

    private void pullDefaultSchedule(final SQLiteDatabase db) {

        schRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ContentValues contentvalues;
                // weekday
                for (DataSnapshot weekdayChild : dataSnapshot.getChildren()) {
                    // hour
                    for (DataSnapshot hourChild : weekdayChild.getChildren()) {
                        contentvalues = new ContentValues();
                        contentvalues.put(DEF_SCHEDULE_COLUMN_WEEKDAY,
                                Integer.valueOf(weekdayChild.getKey()));
                        contentvalues.put(DEF_SCHEDULE_COLUMN_HOUR, hourChild.getKey());
                        db.insert(TABLE_NAME_DEF_SCHEDULE, null, contentvalues);

                        // section
                        for (DataSnapshot sectionChild : hourChild.getChildren()) {
                            // finding max value of ID
                            SQLiteStatement statement = db.compileStatement(SQL_DEF_SCHD_LASTID);
                            int maxID = (int) statement.simpleQueryForLong();
                            // TODO: add trainer ID to make it complete
                            contentvalues = new ContentValues();
                            contentvalues.put(SECTION_ADD_COLUMN_SECTION_ID,
                                    Integer.valueOf(sectionChild.child(FRBS_SECTION_ID).getValue().toString()));
                            // TODO: here should be id TRAINER
//                            contentvalues.put(SECTION_ADD_COLUMN_TRAINER_ID, 1);
                            contentvalues.put(SECTION_ADD_COLUMN_DEF_SCHEDULE_ID, maxID);

                            db.insert(TABLE_NAME_SECTION_ADDITIONAL, SECTION_ADD_COLUMN_TRAINER_ID, contentvalues);
                        }
                    }
                }

               startMainActivity();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, String.valueOf(databaseError.toException()));
            }
        });
    }

    private void startMainActivity() {
        // Start home activity
        startActivity(new Intent(SplashActivity.this, MainActivity.class));
        // close splash activity
        finish();
    }
}
