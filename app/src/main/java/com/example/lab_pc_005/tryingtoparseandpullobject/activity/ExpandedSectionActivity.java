package com.example.lab_pc_005.tryingtoparseandpullobject.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lab_pc_005.tryingtoparseandpullobject.R;
import com.example.lab_pc_005.tryingtoparseandpullobject.util.DARFITDBHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTIONS_COLUMN_DESCRIPTION;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTIONS_COLUMN_NAME;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TABLE_NAME_SECTIONS;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TAG;

public class ExpandedSectionActivity extends AppCompatActivity {

    @BindView(R.id.v_sections_image_exp) ImageView image;
    @BindView(R.id.v_sections_name_exp) TextView sectionName;
    @BindView(R.id.v_sections_desc_exp) TextView description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expanded_section);
        ButterKnife.bind(this);
        Intent intent = getIntent();

        // TODO: add image

        Cursor c = getSectionsCursor(intent.getIntExtra("ID", -1));
        Log.d(TAG, String.valueOf(intent.getIntExtra("ID", -1)));
        DARFITDBHelper.logCursor(c);

        if(c.moveToFirst()){
            sectionName.setText(c.getString(c.getColumnIndex(SECTIONS_COLUMN_NAME)));
            description.setText(c.getString(c.getColumnIndex(SECTIONS_COLUMN_DESCRIPTION)));
        }

        c.close();
    }

    private Cursor getSectionsCursor(int ID){
        return DARFITDBHelper.getCursorById((new DARFITDBHelper(this)).getReadableDatabase(),
                TABLE_NAME_SECTIONS, ID);
    }

    @OnClick(R.id.backButton)
    public void backButtonPressed() {
        finish();
    }
}
