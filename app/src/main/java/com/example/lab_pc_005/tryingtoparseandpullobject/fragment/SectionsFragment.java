package com.example.lab_pc_005.tryingtoparseandpullobject.fragment;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lab_pc_005.tryingtoparseandpullobject.adapter.SectionsRVAdapter;
import com.example.lab_pc_005.tryingtoparseandpullobject.R;
import com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras;
import com.example.lab_pc_005.tryingtoparseandpullobject.entry.Section;
import com.example.lab_pc_005.tryingtoparseandpullobject.util.DARFITDBHelper;

import java.util.ArrayList;

import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTIONS_COLUMN_DESCRIPTION;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTIONS_COLUMN_ID;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTIONS_COLUMN_NAME;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.SECTIONS_COLUMN_URL_MAIN;


/**
 * A simple {@link Fragment} subclass.
 */
public class SectionsFragment extends Fragment {

    private DARFITDBHelper dbHelper;
    private SectionsRVAdapter adapter;
    private ArrayList<Section> arrayOfSections;

    public SectionsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_sections, container, false);
        arrayOfSections = new ArrayList<>();
        adapter = new SectionsRVAdapter(arrayOfSections, rootView.getContext());
        dbHelper = new DARFITDBHelper(rootView.getContext());
        RecyclerView rv = (RecyclerView) rootView.findViewById(R.id.rv_sections);

        rv.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        rv.setHasFixedSize(true);
        rv.setAdapter(adapter);

        fillArrayOfSections();

        return rootView;
    }

    private void fillArrayOfSections() {
        Cursor c = DARFITDBHelper.getSectionsCursor(dbHelper.getReadableDatabase());
        if(c.moveToFirst()) {
            do {
                arrayOfSections
                        .add(new Section(
                                c.getInt(c.getColumnIndex(SECTIONS_COLUMN_ID)),
                                c.getString(c.getColumnIndex(SECTIONS_COLUMN_NAME)),
                                c.getString(c.getColumnIndex(SECTIONS_COLUMN_DESCRIPTION)),
                                c.getString(c.getColumnIndex(SECTIONS_COLUMN_URL_MAIN))));
            } while (c.moveToNext());

            adapter.notifyDataSetChanged();
        }

    }

}
