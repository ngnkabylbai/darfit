package com.example.lab_pc_005.tryingtoparseandpullobject.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lab_pc_005.tryingtoparseandpullobject.R;
import com.example.lab_pc_005.tryingtoparseandpullobject.activity.ExpandedSectionActivity;
import com.example.lab_pc_005.tryingtoparseandpullobject.entry.Section;

import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TAG;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lab-pc-005 on 6/21/17.
 */

public class SectionsRVAdapter extends RecyclerView.Adapter<SectionsRVAdapter.SectionsViewHolder> {

    private ArrayList<Section> arrayOfSections;
    private Context context;

    public SectionsRVAdapter(ArrayList<Section> arrayOfSections, Context context) {
        this.arrayOfSections = arrayOfSections;
        this.context = context;
    }

    @Override
    public SectionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SectionsViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_less_sections, parent, false));
    }

    @Override
    public void onBindViewHolder(SectionsViewHolder holder, int position) {
        ((TextView) holder.sectionName).setText(arrayOfSections.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return arrayOfSections.size();
    }

    private ArrayList<Section> getArrayOfSections() {
        return arrayOfSections;
    }

    private Context getContext() {
        return context;
    }

    // Holder class
    class SectionsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.v_sections_logo_less) ImageView image;
        @BindView(R.id.v_sections_name_less) TextView sectionName;
        @BindView(R.id.v_sections_desc_less) TextView description;

        SectionsViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //TODO : update intent
                    Intent intent = new Intent(getContext(), ExpandedSectionActivity.class);
                    Section curSection = getArrayOfSections().get(getAdapterPosition());

                    intent.putExtra("ID", curSection.getID());
                    Log.d(TAG, String.valueOf(curSection.getID()));
                    getContext().startActivity(intent);
                }
            });
        }

    }
}
