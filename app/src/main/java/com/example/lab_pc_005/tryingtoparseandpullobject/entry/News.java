package com.example.lab_pc_005.tryingtoparseandpullobject.entry;

/**
 * Created by lab-pc-005 on 6/5/17.
 */

public class News {

    private String title;
    private String author;
    private String content;
    private String dateTime;
    private String imageURL;

    public News() {

    }

    public News(String title, String author, String content, String dateTime, String imageURL) {
        this.title = title;
        this.author = author;
        this.content = content;
        this.dateTime = dateTime;
        this.imageURL = imageURL;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getContent() {
        return content;
    }

    public String getDateTime() {
        return dateTime;
    }

    public String getImageURL() {
        return imageURL;
    }

    @Override
    public String toString() {
        return "News{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", content='" + content + '\'' +
                ", dateTime='" + dateTime + '\'' +
                ", imageURL='" + imageURL + '\'' +
                '}';
    }
}
