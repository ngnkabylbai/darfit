package com.example.lab_pc_005.tryingtoparseandpullobject.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lab_pc_005.tryingtoparseandpullobject.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.REQUEST_CODE;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.USER_NAME;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TAG;

public class SignUpActivity extends AppCompatActivity {

    @BindView(R.id.v_sign_up_name) EditText nameHolder;
    @BindView(R.id.v_sign_up_email) EditText emailHolder;
    @BindView(R.id.v_sign_up_pass) EditText passHolder;
    @BindView(R.id.v_sign_up_re_pass) EditText rePassHolder;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        mAuth = FirebaseAuth.getInstance();
    }

    @OnClick(R.id.btn_sign_up)
    public void onClick() {
        final String name = nameHolder.getText().toString();
        final String email = emailHolder.getText().toString();
        final String pass = passHolder.getText().toString();
        final String rePass = rePassHolder.getText().toString();

        if (pass.equals(rePass)) {
            mAuth.createUserWithEmailAndPassword(email, pass)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()) {
                                Toast.makeText(SignUpActivity.this, "Добро пожаловать, " + email, Toast.LENGTH_SHORT).show();
                                Log.d(TAG, "Signing up a new User: " + email + ", " + pass + ", " + name);

                                Intent intent = new Intent(getBaseContext(), SignInActivity.class);
                                intent.putExtra(USER_NAME, name);
                                setResult(REQUEST_CODE, intent);
                                finish();
                            } else {
                                Toast.makeText(SignUpActivity.this, "Неверное имя или пароль", Toast.LENGTH_SHORT).show();
                                Log.d(TAG, "Signing up FAIL: " + email + ", " + pass);
                            }
                        }
                    });
        }
    }
}
