package com.example.lab_pc_005.tryingtoparseandpullobject.activity;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lab_pc_005.tryingtoparseandpullobject.R;
import com.example.lab_pc_005.tryingtoparseandpullobject.util.DARFITDBHelper;

import java.io.BufferedReader;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TABLE_NAME_SECTIONS;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TABLE_NAME_TRAINERS;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_DESCRIPTION;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_NAME;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_PHONE_NUMBER;
import static com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras.TRAINERS_COLUMN_SURNAME;

public class ExpandedTrainerActivity extends AppCompatActivity {

    @BindView(R.id.v_trainers_exp_image) ImageView profilePhoto;
    @BindView(R.id.v_trainers_exp_fullName) TextView fullName;
    @BindView(R.id.v_trainers_exp_description) TextView description;
    @BindView(R.id.v_trainers_exp_phoneNumber) TextView phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expanded_trainer);
        ButterKnife.bind(this);

        Intent intent = getIntent();

        // TODO: add an image
        Cursor c = getTrainersCursor(intent.getIntExtra("ID", -1));
        if(c.moveToFirst()) {
            fullName.setText(c.getString(c.getColumnIndex(TRAINERS_COLUMN_NAME))
                    + " " + c.getString(c.getColumnIndex(TRAINERS_COLUMN_SURNAME)));
            description.setText(c.getString(c.getColumnIndex(TRAINERS_COLUMN_DESCRIPTION)));
            phoneNumber.setText(c.getString(c.getColumnIndex(TRAINERS_COLUMN_PHONE_NUMBER)));
        }
    }

    private Cursor getTrainersCursor(int ID){
        return DARFITDBHelper.getCursorById((new DARFITDBHelper(this)).getReadableDatabase(),
                TABLE_NAME_TRAINERS, ID);
    }

    @OnClick(R.id.backButton)
    public void backButtonPressed() {
        finish();
    }
}
