package com.example.lab_pc_005.tryingtoparseandpullobject.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.lab_pc_005.tryingtoparseandpullobject.R;
import com.example.lab_pc_005.tryingtoparseandpullobject.activity.ExpandedNewsActivity;
import com.example.lab_pc_005.tryingtoparseandpullobject.util.StringExtras;
import com.example.lab_pc_005.tryingtoparseandpullobject.entry.News;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lab-pc-005 on 6/5/17.
 */

public class NewsRVAdapter extends RecyclerView.Adapter<NewsRVAdapter.NewsViewHolder> {

    private ArrayList<News> arrayOfNews;
    private Context context;

    public NewsRVAdapter(ArrayList<News> arrayOfNews, Context context) {
        this.arrayOfNews = arrayOfNews;
        this.context = context;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NewsViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_less_news, parent, false));
    }

    @Override
    public void onBindViewHolder(final NewsViewHolder holder, int position) {
        holder.titleView.setText(arrayOfNews.get(position).getTitle());
        holder.contentView.setText(arrayOfNews.get(position).getContent());

        Glide.with(getContext())
                .load(arrayOfNews.get(position).getImageURL())
                .centerCrop()
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return arrayOfNews.size();
    }

    private Context getContext() {
        return context;
    }

    private ArrayList<News> getArrayOfNews() {
        return arrayOfNews;
    }

    // Holder class
    class NewsViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {

        @BindView(R.id.v_news_title_less) TextView titleView;
        @BindView(R.id.v_news_content_less) TextView contentView;
        @BindView(R.id.v_news_image_less) ImageView imageView;

        NewsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            News clickedNews = getArrayOfNews().get(getAdapterPosition());
            Context context = getContext();
            Intent intent = new Intent(context, ExpandedNewsActivity.class);

            // TODO: reconstruct
            intent.putExtra("title", clickedNews.getTitle());
            intent.putExtra("author", clickedNews.getAuthor());
            intent.putExtra("content", clickedNews.getContent());
            intent.putExtra("dateTime", clickedNews.getDateTime());
            intent.putExtra("imageURL", clickedNews.getImageURL());

            context.startActivity(intent);
        }

    }

}
