package com.example.lab_pc_005.tryingtoparseandpullobject.entry;

/**
 * Created by lab-pc-005 on 6/15/17.
 */

public class Trainer {

    private int id;
    private String name;
    private String surname;
    private String description;
    private String phoneNumber;
    private String URLMain;

    public Trainer() {

    }

    public Trainer(int id, String name, String surname, String description, String phoneNumber,
                   String URLMain) {
        this.id = id;
        this.phoneNumber = phoneNumber;
        this.name = name;
        this.surname = surname;
        this.description = description;
        this.URLMain = URLMain;
    }

    public int getID() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getDescription() {
        return description;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getURLMain() {
        return URLMain;
    }

    @Override
    public String toString() {
        return "Trainer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", description='" + description + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}
